#!/bin/sh

# Set up all the mysqldump variables
FILE0=seafile_db.`date +"%Y-%m-%d-%H-%M-%S"`
FILE1=seahub_db.`date +"%Y-%m-%d-%H-%M-%S"`
FILE2=ccnet_db.`date +"%Y-%m-%d-%H-%M-%S"`
DBSERVER=localhost
DATABASE0=seafile_db
DATABASE1=seahub_db
DATABASE2=ccnet_db
USER=root

unalias rm     2> /dev/null

# Use this for a database server on a separate host:
# mysqldump --opt --protocol=TCP --user=${USER} --host=${DBSERVER} ${DATABASE} > ${FILE}

mysqldump --opt --user=${USER} ${DATABASE0} > ${FILE0}
mysqldump --opt --user=${USER} ${DATABASE1} > ${FILE1}
mysqldump --opt --user=${USER} ${DATABASE2} > ${FILE2}

# (4) gzip the mysql database dump file
gzip $FILE0
gzip $FILE1
gzip $FILE2

# Rsync to backup server
rsync -avzHP --delete $FILE0.gz $FILE1.gz $FILE2.gz root@io.dev:/root/seafile-backup/db/ --log-file=db.log

# Finds files ending in .gz older than 12 days and deletes them
find /root/seafile-backup -mtime +12 -type f -name '*.gz' -delete


sleep 1

# rsync seafile data folder
rsync -avzHP --delete /opt/seafile root@domain.com:/root/seafile-backup/data/ --log-file=seafile-backup-data.log

